#include <Servo.h>
#include <Ultrasonic.h>
#include <LiquidCrystal.h>
#include <String.h>
#include <stdio.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

#define SERVO_SAIDA 51 
#define SERVO_ENTRADA 37 

#define TRIGGER_PIN_SAIDA  31 
#define ECHO_PIN_SAIDA     42 

#define TRIGGER_PIN_ENTRADA 49 
#define ECHO_PIN_ENTRADA     47 

#define TRIGGER_PIN_VAGA1 39
#define ECHO_PIN_VAGA1     38 

#define TRIGGER_PIN_VAGA2 28
#define ECHO_PIN_VAGA2 30

#define TRIGGER_PIN_VAGA3 26
#define ECHO_PIN_VAGA3 24


#define DISTANCIA_ABRE_CANCELA_SAIDA 5
#define DISTANCIA_ABRE_CANCELA_ENTRADA 5
#define DISTANCIA_VAGA 3.5
#define MAXIMO_VAGAS 3

#define TIME_DALAY 60
#define NUMERO_VAGAS 3
#define QTDE_CONTROLE_LEDS 1

Ultrasonic ultrasonic_entrada(TRIGGER_PIN_ENTRADA, ECHO_PIN_ENTRADA);
Ultrasonic ultrasonic_saida(TRIGGER_PIN_SAIDA, ECHO_PIN_SAIDA);

Ultrasonic ultrasonic_vaga[NUMERO_VAGAS] = {

  Ultrasonic(TRIGGER_PIN_VAGA1, ECHO_PIN_VAGA1),
  Ultrasonic(TRIGGER_PIN_VAGA2, ECHO_PIN_VAGA2),
  Ultrasonic(TRIGGER_PIN_VAGA3, ECHO_PIN_VAGA3),
};

Servo s; // Servo saida
Servo e; // Servo Entrada
int qtd_vagas;
int agendado_vaga[NUMERO_VAGAS];

int vaga_ocupada[NUMERO_VAGAS];

int mudarStatusVerde[NUMERO_VAGAS];
int mudarStatusVermelho[NUMERO_VAGAS];

int ledPinVermelhoVaga[NUMERO_VAGAS] = {
  ledPinVermelhoVaga[0] = 50,
  ledPinVermelhoVaga[1] = 43,
  ledPinVermelhoVaga[2] = 34,
};


int ledPinVerdeVaga[NUMERO_VAGAS] = {
  ledPinVerdeVaga[0] = 53,
  ledPinVerdeVaga[1] = 46,
  ledPinVerdeVaga[2] = 36,
};

int ledPinAmareloVaga[NUMERO_VAGAS] = {
  ledPinAmareloVaga[0] = 52,
  ledPinAmareloVaga[1] = 23,
  ledPinAmareloVaga[2] = 41,
};



int incomingByte;

double cmMsec_vaga[NUMERO_VAGAS];
float cmMsec_saida;
float cmMsec_entrada;

int BUZZER = 48;

int length = 15; 
char notes[] = "ccggaagffeeddc "; 
int beats[] = { 
  1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 20;

long microsec_vaga[NUMERO_VAGAS];
long microsec_saida;

void setup ()
{

  Serial.begin(9600);

  lcd.begin(16, 2);              
  s.attach(SERVO_SAIDA);
  e.attach(SERVO_ENTRADA);

  // e.write(90);
  e.write(0);
  for(int i=0;i<NUMERO_VAGAS;i++){
    agendado_vaga[i] = 0;
    vaga_ocupada[i] = 0;
    pinMode(ledPinVerdeVaga[i], OUTPUT);
    pinMode(ledPinAmareloVaga[i], OUTPUT);
    pinMode(ledPinVermelhoVaga[i], OUTPUT);
    mudarStatusVerde[i]=0;
    mudarStatusVermelho[i]=0;  
  }
  qtd_vagas = 0;


  pinMode(TRIGGER_PIN_VAGA3, OUTPUT);
  pinMode(ECHO_PIN_VAGA3, INPUT);

  pinMode(TRIGGER_PIN_VAGA2, OUTPUT);
  pinMode(ECHO_PIN_VAGA2, INPUT);

  pinMode(TRIGGER_PIN_VAGA1, OUTPUT);
  pinMode(ECHO_PIN_VAGA1, INPUT);
  //speak 
  pinMode(BUZZER, OUTPUT);

}

void loop()
{
  lcd.setCursor(0,0);
  lcd.print("OCUPADAS:  "); 
  lcd.setCursor(11,0);            // move cursor to second line "1" and 9 spaces over
  lcd.print(qtd_vagas);      // display seconds elapsed since power-up

  lcd.setCursor(0,1);
  lcd.print("LIVRES  :  ");
  lcd.setCursor(11,1);            // move cursor to second line "1" and 9 spaces over
  lcd.print(MAXIMO_VAGAS-qtd_vagas);   

  // Agendar vaga 1
  if (Serial.available() > 0) {
    incomingByte = serReadInt() ;
    String retorno = converteIntToString(incomingByte);
    char reserva[] = {
      retorno[(retorno.length()-1)]                            };   
    int reservado =  atoi(reserva);
    int vagaRetorno;
    //status
    if(reservado==3){
      for(int i=0; i<NUMERO_VAGAS;i++){
        if(agendado_vaga[i]==1){
          Serial.println("2");
        }
        else{
          Serial.println(vaga_ocupada[i]);
        }
      }
    }
    //Reservado ou cancelar Reserva
    else if(reservado==0 || reservado==1){
      if(retorno.length()>1){
        char cVagaretorno[(retorno.length()-2)];
        for(int i=0;i<(retorno.length()-1);i++){
          cVagaretorno[i] = retorno[i];
        }
        vagaRetorno =  atoi(cVagaretorno);   
      }
      if(vagaRetorno>0 && vagaRetorno<=NUMERO_VAGAS){
        if(reservado==1 && vaga_ocupada[vagaRetorno-1] == 0){    
          agendado_vaga[vagaRetorno-1] = reservado;
        }
        else if(reservado==0 && (agendado_vaga[vagaRetorno-1] == 1)){
          agendado_vaga[vagaRetorno-1] = reservado;        
        }
      }
    }
    else if(reservado==5 ){

      if(retorno.length()>1){
        char cVagaretorno[(retorno.length()-2)];
        for(int i=0;i<(retorno.length()-1);i++){
          cVagaretorno[i] = retorno[i];
        }
        vagaRetorno =  atoi(cVagaretorno);   
      }
      if(vagaRetorno>0 && vagaRetorno<=NUMERO_VAGAS && agendado_vaga[vagaRetorno-1] ==1 ){    
        agendado_vaga[vagaRetorno-1]=0;
        qtd_vagas-=1; 
        delay(1500);
        cancelaEntrada();
        delay(500);
        digitalWrite(ledPinAmareloVaga[vagaRetorno-1], LOW);
        digitalWrite(ledPinVermelhoVaga[vagaRetorno-1], LOW);
        while(true){
          digitalWrite(ledPinVerdeVaga[vagaRetorno-1], HIGH);
          digitalWrite(ledPinAmareloVaga[vagaRetorno-1], HIGH);
          digitalWrite(ledPinVermelhoVaga[vagaRetorno-1], HIGH);

          delay(500);
          digitalWrite(ledPinVerdeVaga[vagaRetorno-1], LOW);
          digitalWrite(ledPinAmareloVaga[vagaRetorno-1], LOW);
          digitalWrite(ledPinVermelhoVaga[vagaRetorno-1], LOW);
          delay(TIME_DALAY);
          microsec_vaga[vagaRetorno-1] = ultrasonic_vaga[vagaRetorno-1].timing();
          cmMsec_vaga[vagaRetorno-1] = ultrasonic_vaga[vagaRetorno-1].convert(microsec_vaga[vagaRetorno-1], Ultrasonic::CM);
          if(cmMsec_vaga[vagaRetorno-1]>0.5 && cmMsec_vaga[vagaRetorno-1]<7.5){
            digitalWrite(ledPinVerdeVaga[vagaRetorno-1], LOW);
            digitalWrite(ledPinVermelhoVaga[vagaRetorno-1], HIGH);

            break;
          }
        }
      }
    }
  }

  /* VAGAS */
  verificarVagas();
  delay(100);
  // CONDICAO PARA ABRIR A CANCELA DA ENTRADA
  cancelaEntrada();
  // CONDICAO PARA ABRIR A CANCELA DA SAIDA 
  cancelaSaida();

  if ( qtd_vagas == MAXIMO_VAGAS && ((cmMsec_entrada>1.5) && (cmMsec_entrada<=DISTANCIA_ABRE_CANCELA_ENTRADA))) {
    delay(TIME_DALAY);
    long microsec_entrada = ultrasonic_entrada.timing();
    cmMsec_entrada = ultrasonic_entrada.convert(microsec_entrada, Ultrasonic::CM);
    avisoCarroVaga();
  }
  qtd_vagas = 0;
  atualizarValores();
}

void atualizarValores(){
  for(int i=0;i<NUMERO_VAGAS;i++){
    qtd_vagas += vaga_ocupada[i] + agendado_vaga[i];
  }
}

String converteIntToString(int x){
  char y[7];  
  sprintf(y, "%i", x);  
  return y;
}

int serReadInt()
{
  int i, serAva;                         
  char inputBytes [7];               
  char * inputBytesPtr = &inputBytes[0]; 

  if (Serial.available()>0)           
  {
    delay(5);                              
    serAva = Serial.available();  
    for (i=0; i<serAva; i++)       
      inputBytes[i] = (char) Serial.read();

    return atoi(inputBytesPtr);   
  }
  else
    return -1;                         
}

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(BUZZER, HIGH);
    delayMicroseconds(tone);
    digitalWrite(BUZZER, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 
    'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C', 'D'                     };
  int tones[] = { 
    1915, 1700, 1519, 1432, 1275, 1136, 1014, 956, 786                     };

  // toque o tom correspondente ao nome da nota
  for (int i = 0; i < 3; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void avisoCarroVaga() {
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } 
    else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pausa entre notas
    delay(tempo / 2); 
  }
}

void cancelaSaida() {
  delay(TIME_DALAY);
  microsec_saida = ultrasonic_saida.timing();
  cmMsec_saida = ultrasonic_saida.convert(microsec_saida, Ultrasonic::CM);

  if( ((cmMsec_saida>0) && (cmMsec_saida<8))){ 
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("CANCELA SAIDA"); 
    lcd.setCursor(0,1);
    lcd.print("ABERTA!");      
    s.write(0);
    while(cmMsec_saida<8){
      microsec_saida = ultrasonic_saida.timing();
      cmMsec_saida = ultrasonic_saida.convert(microsec_saida, Ultrasonic::CM);
      delay(500);
    }
    delay(3000);
    s.write(90);
    lcd.clear();
  } 
}
void cancelaEntrada() {

  delay(TIME_DALAY);
  long microsec_entrada = ultrasonic_entrada.timing();
  cmMsec_entrada = ultrasonic_entrada.convert(microsec_entrada, Ultrasonic::CM);

  if( ((cmMsec_entrada>0) && (cmMsec_entrada<=8) && qtd_vagas < MAXIMO_VAGAS) ){
    lcd.clear();                                                                                                                                                                                                                                                                                  
    lcd.setCursor(0,0);
    lcd.print("CANCELA ENTRADA"); 
    lcd.setCursor(0,1);
    lcd.print("ABERTA!");     
    e.write(90);
    while(cmMsec_entrada<17){
      microsec_entrada = ultrasonic_entrada.timing();
      cmMsec_entrada = ultrasonic_entrada.convert(microsec_entrada, Ultrasonic::CM);
      delay(500);

    }
    e.write(0);
    lcd.clear();
  } 
}

void verificarVagas(){
  // CONDICOES DA VAGA 1
  for(int i=0;i<NUMERO_VAGAS;i++){
    if(agendado_vaga[i] ==1){
      digitalWrite(ledPinVerdeVaga[i], LOW);
      digitalWrite(ledPinVermelhoVaga[i], LOW);
      digitalWrite(ledPinAmareloVaga[i], HIGH);
    }
    else{
      delay(TIME_DALAY);
      microsec_vaga[i] = ultrasonic_vaga[i].timing();
      cmMsec_vaga[i] = ultrasonic_vaga[i].convert(microsec_vaga[i], Ultrasonic::CM);

      if(cmMsec_vaga[i]>0.5 && cmMsec_vaga[i]<7.5){
        mudarStatusVermelho[i]++;
        mudarStatusVerde[i]=0;
        if(mudarStatusVermelho[i]>QTDE_CONTROLE_LEDS){
          vaga_ocupada[i] = 1;
          digitalWrite(ledPinVermelhoVaga[i], HIGH);
          digitalWrite(ledPinVerdeVaga[i], LOW);  
          digitalWrite(ledPinAmareloVaga[i], LOW);     
        }
      }
      else{
        mudarStatusVermelho[i]=0;
        mudarStatusVerde[i]++;
        if(mudarStatusVerde[i]>QTDE_CONTROLE_LEDS){
          digitalWrite(ledPinVermelhoVaga[i], LOW); 
          digitalWrite(ledPinVerdeVaga[i], HIGH);
          digitalWrite(ledPinAmareloVaga[i], LOW);
          vaga_ocupada[i] = 0;
        }
      }
    }
    delay(TIME_DALAY);
  }
}

